Installation:

1. Download Dropzone.js library from "https://github.com/enyo/dropzone/releases" and place in "/libraries/dropzone" folder. File "dropzone.js" must be available at "/libraries/dropzone/dist/dropzone.js".

2. Enable module "Textarea File Drag'n'Drop".

3. Configure on "/admin/config/media/textarea-file-drag" page.
