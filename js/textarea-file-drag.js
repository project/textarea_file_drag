(function ($, Drupal) {

  Drupal.behaviors.textareaFileDrag = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      once('dropzone', 'textarea.textarea-file-drag', context).forEach(function (element) {
        Drupal.textareaFileDrag.processElement(element);
      });
    }
  };

  Drupal.textareaFileDrag = {
    /**
     * Process textarea element.
     */
    processElement: function (element) {
      var $element = $(element);
      var allowedExtensions = $element.data('allowed-extensions');
      var uploadUrl = $element.data('upload-url');

      if (!allowedExtensions || !uploadUrl) {
        return;
      }

      var acceptedFilesArray = allowedExtensions.split(' ').map(function (extension) {
        return '.' + extension;
      });

      var dropzone = new Dropzone(element, {
        url: uploadUrl,
        paramName: 'file',
        acceptedFiles: acceptedFilesArray.join(','),
        clickable: false,
        chunking: false,
        uploadMultiple: false,
        createImageThumbnails: false,
      });

      dropzone.on('success', function (file, response, event) {
        if (response.status) {
          Drupal.textareaFileDrag.insertTextToCursorPosition(element, response.url);
        }
        else if (response.error) {
          alert(response.error);
        }
      });

      dropzone.on('error', function (file, message, xhr) {
        alert(message);
      });
    },

    /**
     * Insert text to cursor position.
     */
    insertTextToCursorPosition: function (element, text) {
      element.setRangeText(text, element.selectionStart, element.selectionEnd, 'end');
    }
  };

})(jQuery, Drupal);
