<?php

namespace Drupal\textarea_file_drag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class TextareaFileDragSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'textarea_file_drag_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'textarea_file_drag.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $settings_config = $this->config('textarea_file_drag.settings');

    $form['allowed_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed extensions'),
      '#description' => $this->t('Extensions separated by spaces.'),
      '#default_value' => $settings_config->get('allowed_extensions'),
    ];

    $form['upload_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Upload path'),
      '#description' => $this->t('Starts with public://'),
      '#default_value' => $settings_config->get('upload_path'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('textarea_file_drag.settings')
      ->set('allowed_extensions', $form_state->getValue('allowed_extensions'))
      ->set('upload_path', $form_state->getValue('upload_path'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
