<?php

namespace Drupal\textarea_file_drag;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TextareaFileDragController extends ControllerBase {

  /**
   * Upload file.
   */
  public function upload(): Response {
    $file_system = \Drupal::service('file_system');
    $file = \Drupal::request()->files->get('file'); /** @var UploadedFile $file */
    $module_settings = \Drupal::config('textarea_file_drag.settings');
    $allowed_extensions = explode(' ', $module_settings->get('allowed_extensions'));

    if ($file) {
      if (!in_array($file->getClientOriginalExtension(), $allowed_extensions, TRUE)) {
        $response = [
          'status' => 0,
          'error' => $this->t('Extension not allowed.'),
        ];
      }
      else {
        $file_source = $file->getRealPath();
        $upload_path = $module_settings->get('upload_path');
        $file_system->prepareDirectory($upload_path, FileSystemInterface::CREATE_DIRECTORY);
        $file_destination = $upload_path . '/' . textarea_file_drag_sanitize_filename($file->getClientOriginalName());
        $file_destination = $file_system->move($file_source, $file_destination);
        $response = [
          'status' => 1,
          'url' => \Drupal::service('file_url_generator')->generateString($file_destination),
        ];
      }
    }
    else {
      $response = [
        'status' => 0,
      ];
    }

    return new JsonResponse($response);
  }

}
